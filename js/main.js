

var properties = [


{
  type:"Purchase",
  rooms:2,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Aleea Castanilor - Berceni",
  surface:"110mp",
  floor:"1/10",
  code:"4444",
  price:"50.450 ",
  agent:"George Carlin"
},
{
  type:"Purchase",
  rooms:1,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Roma - Baba Novac",
  surface:"110mp",
  floor:"5/10",
  code:"1225",
  price:"80.450 ",
  agent:"Joe Rogan"
},
{
  type:"Purchase",
  rooms:2,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Paris - Baba Novac",
  surface:"90mp",
  floor:"10/10",
  code:"5645",
  price:"80.450 ",
  agent:"Seinfeld"
},
{
  type:"Purchase",
  rooms:3,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Paris - Baba Novac",
  surface:"90mp",
  floor:"2/10",
  code:"5645",
  price:"90.450 ",
  agent:"Bill Burr"
},
{
  type:"Purchase",
  rooms:2,
  pet:"Yes",
  category:"Appartments",
  image:"url(./photos/condo-2618421_640.jpg)",
  adress:"Strada Paris - Baba Novac",
  surface:"90mp",
  floor:"2/10",
  code:"5645",
  price:"90.450 ",
  agent:"Bill Burr"
}
];



function byClass(cls) {
  return document.getElementsByClassName(cls);
}


// Creates the divs that will hold the details of each property
function createDivs(prop) {

  var filtered = document.getElementById("filtered");

  // Div that holds the Image and description of each propertie
  var eachHouse = document.createElement("div");
  eachHouse.setAttribute("class", "eachHouse");
  filtered.appendChild(eachHouse)



  // Create the div whitch holds the image
  var filteredImage = document.createElement("div");
  filteredImage.setAttribute("class", "filteredImage");
  eachHouse.appendChild(filteredImage);
  filteredImage.style.backgroundImage = prop.image;





  // Create the div whitch holds the description of the property
  var houseDescription = document.createElement("div");
  houseDescription.setAttribute("class", "houseDescription");
  eachHouse.appendChild(houseDescription);



// Title of each property
  var propertyTitle = document.createElement("h2");
  houseDescription.appendChild(propertyTitle);
  propertyTitle.innerHTML = prop.category + ", " + prop.rooms + " rooms" + ", " + prop.adress;

// Holds the small description - rooms, surface, floor, code
  var smallDescription = document.createElement("div");
    smallDescription.setAttribute("class", "smallDescription");
  houseDescription.appendChild(smallDescription);

  var roomsSmall = document.createElement("h4");
  smallDescription.appendChild(roomsSmall);
  roomsSmall.innerHTML =  '<i class="fas fa-home" aria-hidden="true"></i>' + " " + prop.rooms +" rooms";

  var surfaceSmall = document.createElement("h4");
  smallDescription.appendChild(surfaceSmall);
  surfaceSmall.innerHTML = '<i class="fas fa-chart-pie"></i>' + " " + prop.surface;


  var floorSmall = document.createElement("h4");
  smallDescription.appendChild(floorSmall);
  floorSmall.innerHTML = '<i class="far fa-building"></i>' + " " + " Floor " + prop.floor;

  var codeSmall = document.createElement("h4");
  smallDescription.appendChild(codeSmall);
  codeSmall.innerHTML =  "Code " +prop.code;



  // Holds price, details button, agent
  var smallDescription2 = document.createElement("div");
  houseDescription.appendChild(smallDescription2);
  smallDescription2.setAttribute("class", "smallDescription2");

  var price = document.createElement("h4");
  price.setAttribute("class", "price")
  smallDescription2.appendChild(price);
  price.innerHTML = prop.price + ' <i class="fa fa-euro-sign" aria-hidden="true"></i>';

  // Gives margins to the div button
  var divButton = document.createElement("div");
  divButton.setAttribute("class", "divButton");
  smallDescription2.appendChild(divButton);

  var priceButton = document.createElement("button");
  priceButton.setAttribute("class", "btn btn-primary detailsButton");
  priceButton.innerHTML = "Details";
  divButton.appendChild(priceButton);

  var agent = document.createElement("div");
  agent.setAttribute("class", "agent");
  smallDescription2.appendChild(agent);


  var agentImg = document.createElement("IMG");
  agentImg.setAttribute("src", "./photos/avatar.png");
  agent.appendChild(agentImg);

  var agentName = document.createElement("a");
  agent.setAttribute("href", "#");
  agentName.innerHTML=prop.agent;
  agent.appendChild(agentName);

}




function showSelected() {

    document.getElementById("filtered").innerHTML = "";
    // Select type from filter
    var selector = document.getElementById('selectType');
    var type = selector[selector.selectedIndex].value;



    // Select rooms from filter
    var selector = document.getElementById('selectRooms');
    var rooms = selector[selector.selectedIndex].value;
    // parseInt(rooms);
    // console.log( typeof rooms);


    // Select pet friendly from filter
    var selector = document.getElementById('petFriendly');
    var petFriendly = selector[selector.selectedIndex].value;


    // Select category from filter
    var selector = document.getElementById('selectCategory');
    var category = selector[selector.selectedIndex].value;


     for (var i = 0; i < properties.length; i++) {
         if ( properties[i].type     == type &&
              properties[i].rooms    == rooms &&
              properties[i].pet      == petFriendly &&
              properties[i].category == category){

              document.getElementById("hideContent").style.display = "none";
              document.getElementById("filtered").style.display = "block";

              createDivs( properties[i] );
         } else {
              console.log("No properties");
         }
       }
     }
